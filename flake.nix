{
  description = "Create isolated environments for matlab";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
    flake-utils.url = "github:numtide/flake-utils";
    nix-matlab.url = "gitlab:doronbehar/nix-matlab";
  };

  outputs = { self, nixpkgs, flake-utils, nix-matlab }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        matlabWrapper = pkgs.callPackage ./wrapper.nix {
          matlab = nix-matlab.packages.${system}.matlab;
        };
      in {
        packages.matlabWrapper = matlabWrapper;
        packages.mkPackage = pkgs.callPackage ./mkPackage.nix {};
        defaultPackage = self.packages.${system}.matlabWrapper;
      });
}
