{ stdenv }:
{ pname, version, src, preBuild ? "", postBuild ? "", preInstall ? ""
, postInstall ? "", checkPhase ? "", preCheck ? "", postCheck ? "", meta ? { }
}:

stdenv.mkDerivation {
  inherit pname version src meta preBuild postBuild preInstall postInstall
    checkPhase preCheck postCheck;

  doUnpack = false;
  installPhase = ''
    runHook preInstall

    mkdir -p "$out"/packages
    cp -r . "$out"/packages/+${pname}

    runHook postInstall
  '';
}
