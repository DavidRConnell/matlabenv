{ symlinkJoin, matlab, packages ? [ ] }:

symlinkJoin {
  name = "${matlab.name}-wrapper";
  preferLocalBuild = true;
  allowSubstitutes = false;

  buildInputs = [ matlab ] ++ packages;
  paths = packages;

  postBuild = ''
    # Make packages directory in case no packages passed to wrapper
    mkdir -p "$out"/{bin,packages}

    cat <<_EOF_ > "$out"/bin/matlab
        ${matlab}/bin/matlab "\$@" -r "restoredefaultpath; addpath(\"$out/packages\");"
    _EOF_

    chmod +x "$out"/bin/matlab
  '';
}
